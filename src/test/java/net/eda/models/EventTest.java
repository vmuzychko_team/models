package net.eda.models;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

import static org.junit.Assert.*;

public class EventTest
{
    @Test
    public void testGetBytes()
    {
        Payload payload = new Payload();
        payload.setDescription("test");
        Event event = new Event(payload);
        byte[] bytes = event.toByteArray();

        Event eventDeserialized = null;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis))
        {
            eventDeserialized = (Event) in.readObject();
        }
        catch (Exception e)
        {

        }

        assertEquals(event, eventDeserialized);
    }

    @Test
    public void testDeserialize() {
        Payload payload = new Payload();
        payload.setDescription("test");
        Event event = new Event(payload);
        byte[] bytes = event.toByteArray();

        Event eventDeserialized = new Event(bytes);
        assertEquals(event, eventDeserialized);
    }
}