package net.eda.models;

import java.io.Serializable;
import java.util.Objects;

public class Payload implements Serializable
{
    private static final long serialVersionUID = -5270463492107811000L;

    private String description;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Payload payload = (Payload) o;
        return Objects.equals(description, payload.description);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(description);
    }

    @Override
    public String toString()
    {
        final StringBuffer sb = new StringBuffer("Payload{");
        sb.append("description='").append(description).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
