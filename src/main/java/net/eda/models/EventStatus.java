package net.eda.models;

import java.util.Objects;

public class EventStatus
{
    private long id;
    private String status;
    private String errorDescription;

    public EventStatus(long id, String status, String errorDescription)
    {
        this.status = status;
        this.errorDescription = errorDescription;
    }

    public String getStatus()
    {
        return status;
    }

    public String getErrorDescription()
    {
        return errorDescription;
    }

    public long getId()
    {
        return id;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        EventStatus that = (EventStatus) o;
        return id == that.id &&
            Objects.equals(status, that.status) &&
            Objects.equals(errorDescription, that.errorDescription);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, status, errorDescription);
    }

    @Override
    public String toString()
    {
        final StringBuffer sb = new StringBuffer("EventStatus{");
        sb.append("id=").append(id);
        sb.append(", status='").append(status).append('\'');
        sb.append(", errorDescription='").append(errorDescription).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
