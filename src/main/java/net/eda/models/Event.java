package net.eda.models;

import net.eda.exception.EdaException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Event implements Serializable
{
    private static final long serialVersionUID = -5322109404917798032L;

    private long id;

    private Payload payload;

    private List<String> validationErrors = new ArrayList<>();

    public Event(long id, Payload payload, List<String> validationErrors)
    {
        this.id = id;
        this.payload = payload;
        this.validationErrors = validationErrors;
    }

    public Event(Payload payload)
    {
        this.id = System.currentTimeMillis();
        this.payload = payload;
    }

    public Event(byte bytes[])
    {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
             ObjectInput in = new ObjectInputStream(bis))
        {
            Event eventDeserialized = (Event) in.readObject();
            this.id = eventDeserialized.getId();
            this.payload = eventDeserialized.getPayload();
            this.validationErrors = eventDeserialized.getValidationErrors();

        }
        catch (IOException | ClassNotFoundException e)
        {
            throw new EdaException("Unable to deserialize event.", e);
        }
    }

    public byte[] toByteArray()
    {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutput out = new ObjectOutputStream(bos))
        {
            out.writeObject(this);
            return bos.toByteArray();
        }
        catch (IOException e)
        {
            throw new EdaException("Unable to serialize event.", e);
        }
    }

    public Payload getPayload()
    {
        return payload;
    }

    public long getId()
    {
        return id;
    }

    public List<String> getValidationErrors()
    {
        return new ArrayList<>(validationErrors);
    }

    public void addValidationError(String error) {
        this.validationErrors.add(error);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Event event = (Event) o;
        return id == event.id &&
            Objects.equals(payload, event.payload) &&
            Objects.equals(validationErrors, event.validationErrors);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, payload, validationErrors);
    }

    @Override
    public String toString()
    {
        final StringBuffer sb = new StringBuffer("Event{");
        sb.append("id=").append(id);
        sb.append(", payload=").append(payload);
        sb.append(", validationErrors=").append(validationErrors);
        sb.append('}');
        return sb.toString();
    }
}
