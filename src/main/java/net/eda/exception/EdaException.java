package net.eda.exception;


public class EdaException extends RuntimeException
{
    public EdaException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
